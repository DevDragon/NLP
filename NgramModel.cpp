#include "NgramModel.hpp"
#include "main.hpp"

#include <algorithm>

NgramModel::NgramModel(int x)
{
	n = x;
}

void NgramModel::update(string sentence)
{
	vector<tuple<vector<string>, string>> grams = ngrams(n, tokenize(sentence));
	for (tuple<vector<string>, string> gram : grams)
	{
		vector<string> context = get<0>(gram);
		string token = get<1>(gram);

		map<vector<string>, map<string, int>>::iterator search = counts.find(context);
		if (search == counts.end())
		{
			map<string, int> count;
			count[token] = 1;
			counts[context] = count;
		}
		else
		{
			map<string, int>::iterator countSearch = counts[context].find(token);
			if (countSearch == counts[context].end())
			{
				counts[context][token] = 1;
			}
			else
			{
				counts[context][token]++;
			}
		}
	}
	return;
}

float NgramModel::prob(vector<string> context, string token)
{
	map<vector<string>, map<string, int>>::iterator search = counts.find(context);
	map<string, int>::iterator countSearch = counts[context].find(token);
	if (search == counts.end() || countSearch == counts[context].end())
	{
		return 0.0;
	}
	else
	{
		float count = 0;
		map<string, int>::iterator it;
		for (it = counts[context].begin(); it != counts[context].end(); it++)
		{
			count += it->second;
		}
		return (counts[context][token] / count);
	}
	return 0.0;
}

string NgramModel::random_token(vector<string> context)
{
	float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
	vector<string> tokens;
	map<string, int>::iterator it;
	for (it = counts[context].begin(); it != counts[context].end(); it++)
	{
		tokens.push_back(it->first);
	}
	sort(tokens.begin(), tokens.end());
	float p = 0;
	for (string token : tokens)
	{
		p += prob(context, token);
		if (p > r)
		{
			return { token };
		}
	}
	return tokens[0];
}

string NgramModel::random_text(int token_count)
{
	string result = "";
	vector<string> context(n - 1, "<START>");
	for (int i = 0; i < token_count; i++)
	{
		if (n == 1)
		{
			result += random_token({}) + " ";
		}
		else
		{
			string element = random_token(context);
			if (element == "<END>")
			{
				context.clear();
				context = vector<string>(n - 1, "<START>");
			}
			else
			{
				context.erase(context.begin());
				context.push_back(element);
			}
			result += element + " ";
		}
	}
	return result;
}

double NgramModel::perplexity(string sentence)
{
	vector<tuple<vector<string>, string>> sgrams = ngrams(n, tokenize(sentence));
	double prod = 1;
	for (tuple<vector<string>, string> gram : sgrams)
	{
		prod *= 1.0 / prob(get<0>(gram), get<1>(gram));
	}
	return pow(prod, 1.0 / sgrams.size());
}