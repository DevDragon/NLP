#pragma once

#include <string>
#include <vector>
#include <map>

using namespace std;

class NgramModel
{
public:
	int n;
	map<vector<string>, map<string, int>> counts;

	NgramModel(int x);

	void update(string sentence);

	float prob(vector<string> context, string token);

	string random_token(vector<string> context);

	string random_text(int token_count);

	double perplexity(string sentence);
};