using namespace std;

#include "main.hpp"
#include "tests.hpp"

#include <vector>
#include <tuple>
#include <string>
#include <iostream>
#include <map>
#include <algorithm>
#include <fstream>

vector<string> tokenize(string text)
{
	vector<string> result;
	string temp = "";

	for (char c : text)
	{
		if (isspace(c))
		{
			if (temp != "")
			{
				result.push_back(temp);
			}
			temp = "";
		}
		else if (ispunct(c))
		{
			if (temp != "")
			{
				result.push_back(temp);
			}
			result.push_back(string(1, c));
			temp = "";
		}
		else
		{
			temp += c;
		}
	}

	if (temp != "")
	{
		result.push_back(temp);
	}

	return result;
}

vector<tuple<vector<string>, string>> ngrams(int n, vector<string> tokens)
{
	vector<tuple<vector<string>, string>> result;

	for (int i = 0; i < n - 1; i++)
	{
		tokens.insert(tokens.begin(), "<START>");
	}
	tokens.push_back("<END>");

	for (int i = n - 1; i < tokens.size(); i++)
	{
		vector<string> context;
		for (int j = i - n + 1; j < i; j++)
		{
			context.push_back(tokens[j]);
		}
		tuple<vector<string>, string> ngram = make_tuple(context, tokens[i]);
		result.push_back(ngram);
	}

	return result;
}

NgramModel create_ngram_model(int n, string path)
{
	NgramModel model = NgramModel(n);
	fstream file;
	file.open(path, ios::in);
	if (file.is_open())
	{
		string line;
		while (getline(file, line))
		{
			model.update(line);
		}
	}
	file.close();
	return model;
}

int main()
{
	//testTokenize("   This is an example.   ");
	//testTokenize("'Medium-rare,' she said.");

	//testNgrams(1, { "a", "b", "c" });
	//testNgrams(3, { "a", "b", "c" });

	//testNgramModelProb(1, { "a b c d", "a b a b" }, {}, "a");
	//testNgramModelProb(1, { "a b c d", "a b a b" }, {}, "c");
	//testNgramModelProb(1, { "a b c d", "a b a b" }, {}, "<END>");
	//testNgramModelProb(2, { "a b c d", "a b a b" }, {"<START>"}, "a");
	//testNgramModelProb(2, { "a b c d", "a b a b" }, {"b"}, "c");
	//testNgramModelProb(2, { "a b c d", "a b a b" }, {"a"}, "x");

	//testRandomToken(1, { "a b c d", "a b a b" }, 1, 25, {});
	//testRandomToken(2, { "a b c d", "a b a b" }, 2, 6, { "<START>" });
	//testRandomToken(2, { "a b c d", "a b a b" }, 2, 6, { "b" });

	//testRandomText(1, { "a b c d", "a b a b" }, 1, 13);
	//testRandomText(2, { "a b c d", "a b a b" }, 2, 15);

	//testPerplexity(1, { "a b c d", "a b a b" }, "a b");
	//testPerplexity(2, { "a b c d", "a b a b" }, "a b");

	testNgramModel(1, "Training/frankenstein.txt", 15);
	testNgramModel(2, "Training/frankenstein.txt", 15);
	testNgramModel(3, "Training/frankenstein.txt", 15);
	testNgramModel(4, "Training/frankenstein.txt", 15);

	return 0;
}