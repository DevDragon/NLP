#pragma once

#include "NgramModel.hpp"

#include <vector>
#include <string>

using namespace std;

vector<string> tokenize(string text);

vector<tuple<vector<string>, string>> ngrams(int n, vector<string> tokens);

NgramModel create_ngram_model(int n, string path);