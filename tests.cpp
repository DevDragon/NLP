#include "tests.hpp"
#include "main.hpp"

#include <iostream>

void testTokenize(string sentence)
{
	cout << "TESTING TOKENIZE(): \"" << sentence << "\"" << endl;
	vector<string> testOutput1 = tokenize(sentence);
	cout << "[ ";
	for (string s : testOutput1)
	{
		cout << "\"" << s << "\"" << " ";
	}
	cout << "]" << endl;
}

void testNgrams(int n, vector<string> tokens)
{
	cout << "TESTING NGRAMS(): " << n << ", { ";
	for (string s : tokens)
	{
		cout << s << " ";
	}
	cout << "}" << endl;

	vector<tuple<vector<string>, string>> grams = ngrams(n, tokens);
	cout << "{ ";
	for (tuple<vector<string>, string> ngram : grams)
	{
		cout << "( ";
		cout << "{ ";
		for (string s : get<0>(ngram))
		{
			cout << s << " ";
		}
		cout << "} ";
		cout << get<1>(ngram) << " ) ";
	}
	cout << "}" << endl;
}

void testNgramModelProb(int n, vector<string> updates, vector<string> context, string token)
{
	cout << "TESTING NGRAMMODEL PROB():" << endl;
	NgramModel model = NgramModel(n);
	for (string s : updates)
	{
		model.update(s);
	}
	cout << model.prob(context, token) << endl;
}

void testRandomToken(int modelSize, vector<string> updates, int seed, int n, vector<string> context)
{
	cout << "TESTING RANDOM TOKEN:" << endl;
	NgramModel model = NgramModel(modelSize);
	for (string s : updates)
	{
		model.update(s);
	}
	srand(seed);
	for (int i = 0; i < n; i++)
	{
		cout << model.random_token(context) << endl;
	}
}

void testRandomText(int modelSize, vector<string> updates, int seed, int n)
{
	cout << "TESTING RANDOM TEXT:" << endl;
	NgramModel model = NgramModel(modelSize);
	for (string s : updates)
	{
		model.update(s);
	}
	srand(seed);
	cout << model.random_text(n) << endl;
}

void testPerplexity(int modelSize, vector<string> updates, string sentence)
{
	cout << "TESTING PERPLEXITY:" << endl;
	NgramModel model = NgramModel(modelSize);
	for (string s : updates)
	{
		model.update(s);
	}
	cout << model.perplexity(sentence) << endl;
}

void testNgramModel(int modelSize, string trainingPath, int n)
{
	cout << "TESTING CREATE NGRAM MODEL" << endl;
	NgramModel model = create_ngram_model(modelSize, trainingPath);
	cout << model.random_text(n) << endl;
}