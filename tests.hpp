#pragma once

#include <string>
#include <vector>

using namespace std;

void testTokenize(string sentence);

void testNgrams(int n, vector<string> tokens);

void testNgramModelProb(int n, vector<string> updates, vector<string> context, string token);

void testRandomToken(int modelSize, vector<string> updates, int seed, int n, vector<string> context);

void testRandomText(int modelSize, vector<string> updates, int seed, int n);

void testPerplexity(int modelSize, vector<string> updates, string sentence);

void testNgramModel(int modelSize, string trainingPath, int n);